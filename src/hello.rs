#[link(name = "hello")]
extern {
    pub fn c_hello_world();
}

pub fn hello_world() {
    unsafe {
        c_hello_world();
    }
}

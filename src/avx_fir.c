#include <immintrin.h>

typedef union u128_f {
  __m128 v;
  float a[4];
} U128_F;

typedef union u256_f {
  __m256 v;
  float a[8];
} U256_F;

typedef struct {
  U256_F taps[8];
  U256_F buffer[8];
} FirFilter;

// Initialize a new FIR filter structure on stack
FirFilter fir_init(float taps[8]) {

  FirFilter f;
  for (size_t i = 0; i < 8; i++) {
    f.taps[i].v = _mm256_set_ps(
                  taps[8 * i],
                  taps[8 * i + 1],
                  taps[8 * i + 2],
                  taps[8 * i + 3],
                  taps[8 * i + 4],
                  taps[8 * i + 5],
                  taps[8 * i + 6],
                  taps[8 * i + 7]);
    f.buffer[i].v = _mm256_setzero_ps();
  }
}

// Push a new value into our filter
float fir_push(float input, FirFilter *f) {

  // TODO: Actually shift in new float value to f->buffer somehow

  // Work buffer
  U256_F temp[8];

  // Multiply
  for (size_t i = 0; i < 8; i++) {
    temp[i].v = _mm256_mul_ps(f->taps[i].v, f->buffer[i].v);
  }

  // Summing
  for (size_t i = 0; i < 4; i++) {
    temp[i].v = _mm256_add_ps(temp[2 * i].v, temp[2 * i + 1].v);
  }

  for (size_t i = 0; i < 2; i++) {
    temp[i].v = _mm256_add_ps(temp[2 * i].v, temp[2 * i + 1].v);
  }

  temp[0].v = _mm256_add_ps(temp[0].v, temp[1].v);

  // TODO: Gotta be a SIMD way of doing this
  float output = 0.0;
  for (size_t i = 0; i < 8; i++) {
    output += temp[0].a[i];
  }

  return output;
}

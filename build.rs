fn main() {

    println!("cargo:rerun-if-changed=src/hello.c");

    cc::Build::new()
        .file("src/hello.c")
        .compile("hello");

    cc::Build::new()
        .file("src/avx_fir.c")
        .flag("-mavx")
        .opt_level(2)
        .compile("avx_fir");
}
